#!/bin/bash

OPTION=$1
EXTRA=$2
NAME=neo4j
LINKS=""
COMMAND=""
IMAGE="neo4j"
PORTS="-p 7474:7474 -p 7687:7687"
VOLUMES=""
ENV=""

if [ -z $OPTION ]; then
	OPTION=run
fi

if [ $OPTION = "run" ]; then
	docker run -d $PORTS $ENV --name $NAME $VOLUMES $LINKS $IMAGE $COMMAND
fi

if [ $OPTION = "rm" ]; then
	docker rm -f $NAME
fi

if [ $OPTION = "stop" ]; then
	docker stop $NAME
fi

if [ $OPTION = "start" ]; then
	docker start $NAME
fi

if [ $OPTION = "attach" ]; then
	docker attach $NAME
fi
