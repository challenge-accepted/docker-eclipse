FROM codenvy/ubuntu_jdk8

RUN sudo apt-get update && sudo apt-get install -y libswt-gtk-3-jni libswt-gtk-3-java vim net-tools

WORKDIR /home/user

ADD eclipse.tar.gz /home/user

CMD ["/bin/bash"]
