#!/bin/bash

OPTION=$1
EXTRA=$2
LOCALIP=`ifconfig | awk '/inet/{print substr($2, 0)}' | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b" | grep -vC 0 -oE "\b(127\.)([0-9]{1,3}\.){2}[0-9]{1,3}\b" | head -n +1`
NAME=eclipse
LINKS=""
COMMAND="/bin/bash"
IMAGE="arlindosilvaneto/eclipse"
PORTS="-p 8080:8080"
VOLUMES="-v `pwd`/projects:/home/user/projects -v `pwd`/m2:/home/user/.m2  -v `pwd`/workspace:/home/user/workspace -v `pwd`/eclipse-settings:/home/user/.eclipse"
REPOSITORY="-v /Users/aneto/Dropbox/Public/repository:/repository"
ENV="-e DISPLAY=$LOCALIP:0" # YOUR IP, BUT NOT LOCALHOST

if [ -z $OPTION ]; then
	OPTION=run
fi

if [ $OPTION = "run" ]; then
	docker run --rm -it $PORTS $ENV --name $NAME $VOLUMES $REPOSITORY $LINKS $IMAGE $COMMAND
fi

if [ $OPTION = "rm" ]; then
	docker rm -f $NAME
fi

if [ $OPTION = "stop" ]; then
	docker stop $NAME
fi

if [ $OPTION = "start" ]; then
	docker start $NAME
fi

if [ $OPTION = "attach" ]; then
	docker attach $NAME
fi
